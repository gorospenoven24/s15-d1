// Operators

// assignment opeators
// basic assignment opeartor (=)


let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment opeartor :" + assignmentNumber)

// addition assignment operator(+=)
assignmentNumber +=2;
console.log("Result of addition assignment opeartor :" + assignmentNumber)

// subtraction/ multiplication/ division assignment operator(-=, *=, /=)

// aritmetic operators (-, *, /, %modulo)
let x = 1397;
let y = 7831;

let sum = x + y;
let difference  = x - y;
let product  = x * y;
let quotient  = x / y;
let modulus  = x % y;
console.log("result of addition operator: " + sum);
console.log("result of difference operator: " + difference);
console.log("result of product operator: " + product);
console.log("result of quotient operator: " + quotient);
console.log("result of modulus operator: " + modulus);

//  multiple operators and parentheses

let mdas = 1 + 2-3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);


let pemdas = 1 + (2-3) * (4/5);
console.log(" result of the pemdas operation: " + pemdas);


// Increment and decrement
let z = 1;
let increment = ++z;
console.log("Result of pre- increment: " + increment);
console.log("Result of pre-increment: " + z);

// post-increment
// The value of "z" is return and stored in the variable "increment" then the value of "z" is increased by one
increment = z++;
console.log	("Result of post-increment: " + increment);
console.log	("Result of post-increment: " + z);

// Pre decrement
let decrement = --z;
console.log	("Result of pre-decrement: " + decrement);
console.log	("Result of pre-decrement: " + z);

// post decrement
decrement = z--;
console.log	("Result of pre-decrement: " + decrement);
console.log	("Result of pre-decrement: " + z);

// Type Coercion
// - type coercion is the autoatic or implicit conversion of vaues from one data type to another

let numA = "10";
let	numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);


let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);



// comparison operators

let juan = "Juan"

// equality opeartor (==)
// 	check weher the operands are equal/ have the same content
// 	return a bon value

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
// comapres two string that are the same
console.log('juan' =='juan');
// comapres a string with the variable juan declared above
console.log('juan' == juan);

// Inequality operator (!=)
//  - checks weather the operands are not equal/ have different content
/*
console.log(1 != 1);
console.log(1 !=  2);
console.log(1 !=  '1');
console.log(0 != false);
console.log('juan' != 'juan');
console.log('juan' != juan);
*/
// strict eqaulity opeartor (===)
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === 'juan');
console.log('juan' === juan);

// strict inequality opeartor (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== 'juan');
console.log('juan' !== juan);


// logical operators

let isLegalAge = true;
let isRegistered = false;

// logical and operator (&& - double ampersand)
// Returns true if all operand are true

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// logical Or operator (|| - double pipe)
// return true if one of the operans are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// logical Not operator (! - exclamation point)
// return the opposite value(negates the value)

let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet)

/*Relational opeartors
	< - less than
	> - greater than
	<= - less than or equal to
	>= - greater than or equal to
*/

let a = 5;
let relate = a <= 5;
console.log(relate);

// Selection control structures
// if, else if and else statement


// if statement - execute a statement if a specified condition is true
/* synatx
	if(condition){
	statements;
	}
*/
let numG = -1;
if(numG < 0 ){
	console.log("hello")
}

let numH = 1
// else if clause - execute statement if previous conditions and the specified condition is true

if(numG > 0){
	console.log("hello")
}
else if(numH == 0) {
	console.log("world")
}
else if(numH > 0) {
	console.log("Solar system")
}


//  else statement- executes a statement if all othercnditions are false

if(numG > 0){
	console.log("hello")
}
else if(numH == 0) {
	console.log("world")
}
else {
	console.log("Try Again")
}

/*
	Department A = 1 - 3
	Department B = 4 - 6
	Department C = 7 - 9
	
*/

let dept = 4;
if (dept >= 1 && dept <= 3){
	console.log("your in department A");
}
else if (dept >= 4 && dept <= 6){
	console.log("your in department B");
}
else if (dept >= 7 && dept <= 9){
	console.log("your in department C");
}
else{
	console.log("Department does not exist");
}

let message = "No message";
console.log(message);

function determineTyphoonIntensity(windspeed){
	 if (windspeed < 30) {
	 	return "Not a typhoon yet";

	 }
	 else if (windspeed < 61) {
	 	return "Tropical Depression detected";
	 }
	 else if (windspeed >= 62 && windspeed <= 88) {
	 	return "Tropical storm detected";
	 }
	 else if (windspeed >= 89 && windspeed <= 117) {
	 	return "Severe tropical storm detected";
	 }
	 else{
	 	return "Typhoon Detected";
	 }

}
//  returning the string to the variable 'message'  that invoked it
message = determineTyphoonIntensity(66)
console.log(message);


//  Thruthy  and falsy
// in js a "thruty" value that is considered true when enountered boolean context
/*falsy value
1.false
2. 0
3. -0 
5. null
6 undefined
7. NaN*/

if (true){
	console.log('truthy');
}

if(1){
	console.log('truthy');
}

// Falsy Example
if (false){
	console.log('falsy');
}

if(0){
	console.log('falsy');
}

if(undefined){
	console.log('falsy');
}

//  single statement execution
// conditional ternary operation

/*
syntax
(expression) ? ifTrue : ifFalse;
*/

let ternaryResult = (1 < 18) ? true : false;
console.log("Result of ternary operator: " + ternaryResult);


let name;

function isOfLegalAge(){
	name = 'john';
	return ' you are of the legal age limit';
}
function isUnderAge(){
	name = 'Jane';
	return ' you are under age limit';

}
let age = parseInt(prompt("what is your age?"));
console.log(age);
let legalAge = (age > 18) ? isOfLegalAge() :isUnderAge();
console.log("The result of ternary operator function: " +legalAge + ", " + name);



//  swicth statement
/* syntax
	switch(expression){
		case value1:
			statement/s;
			break;
		case value2:
			statement/s;
			break;
		default:
			statement;

	}

*/

// toLowerCase() method transform the text to lower form
let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day){
		case 'monday':
				console.log("The color of the day is red");
				break;
		case 'tuesday':
				console.log("The color of the day is orange");
				break;
		case 'wednesday':
				console.log("The color of the day is yellow");
				break;
		case 'thursday':
				console.log("The color of the day is green");
				break;
		case 'friday':
				console.log("The color of the day is blue");
				break;
		case 'saturday':
				console.log("The color of the day is indigo");
				break;
		case 'sunday':
				console.log("The color of the day is violet");
				break;	
		default:
		console.log("Please input a valid day");
}



// Try-catch-finally statement
// try catch statement are commonly used for error handling
//  finally block is used to specify a response/ action is used to handle/ resolve errors


function showIntensityAlert(windspeed){
	try{
		alert(determineTyphoonIntensity(windspeed))
	}
	catch (error){
		console.log(typeof error);
	}
	finally {
		alert("Intensity updates will show new alert:");
	}
}

showIntensityAlert(56);
console.log(message);